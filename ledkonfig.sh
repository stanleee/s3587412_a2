#!/bin/bash

#Task 2: Script Launch
function task2(){

cat<< END
Welcome to Konfigurator!
============================
Please select an led to configure:
END
num=1
for led in /sys/class/leds/*; do     #loop through directory and prints available leds
	echo "$num. $(basename $led)"	
	sel["$num"]=$led	#assigned each available led as an array 
	((num++))
done

echo "$num. Quit"

read -p "Please enter a number (1-9) for the led to configure or quit: " input

if ((input==$num));then			#any input will be rejected except 1-6
	exit
elif !(((input >=1))&&((input < $num)));then	
       	echo "Invalid Input! Please try again!"
       	task2
else
	task3 ${sel[$input]}
fi       

}

#Task 3 Manipulation Menu
function task3(){
	 

	echo "$(basename "$1")"
	echo "======================
	What would you like to do with this led?
	1) turn on
	2) turn off
	3) associate with a system event
	4) associate with the performance of a process 
	5) stop association with a process’ performance 
	6) quit to main menu"
	
	read -p  "Please enter a number (1-6) for your choice: " input
	
	case $input in 	
		1)
			task4 $1 1;;
		2)
			task4 $1 0;;
		3)
			task5 $1;;
		4)
			task6 $1;;
		5)
			task7 $1;;
		6)
			echo "Quit to menu"
			task2;;



		*)
			echo "Invalid Input! Please try again"
			task3;;
	esac

}

#Task4: Turn on and off the led
function task4(){
	sudo sh -c "echo $2 > $1/brightness" 
}


#Task5: Associate LED with a system event
function task5(){

	echo "
	Associate LED with a system Event
	=================================
	Available events are:
	----------------------
	"
	#read -r triggers < $1/trigger
	IFS=" " read -ra triggers < "$1/trigger"

	num2=1
	for trigger in "${triggers[@]}" ;do
		first="${trigger:0:1}"
		if [ "$first" == "[" ] ;then
			echo "$num2) ${trigger:1:-1}"
		else	
			echo "$num2) $trigger"
		fi
		sel["$num2"]=$trigger
		((num2++))	
	
	done
	echo "$num2) Quit to previous menu"
	read -p "Please select an option (1-$num2):" input2
	
	

	if ((input2==$num2));then			#any input will be rejected except 1-33
		task3
	elif !(((input2 >=1))&&((input2 < $num2))) ;then	
       		echo "Invalid Input! Please try again!"
		task5
	else
		selection="${sel[$input2]}"
		sudo sh -c "echo $selection > $1/trigger"
		
	fi    

}

function task6(){
	echo "
	Associate LED with the performance of a process 
	-------------------------------------------------"
	read -p "Please enter the name of the program to monitor(partial names are ok): " input3
	
	if [[ $input3 = $(ps -C $input3 | wc -l) ]] ;then
		echo""
		
	fi

		
	read -p "Do you wish to 1) monitor memory or 2) monitor cpu? [enter memory or cpu]: " input4
}

function task7(){
	echo "stop_associate_perf"
}



task2

