# COSC1131/1133 UNIX Systems Administration and Programming

# Assignment 2 – Shell Scripting
---------------------
   
 * Introduction
 * Getting Started
 * Development 
 * Maintainers
 * Useful Resources
 
## Introduction
 -----------------
 In this project user will be able to use scripts provided to control the behavior of the LEDs light on the connected Raspberry PI. 
 This wll be done by sending messages to the device about how to behave.
 
## Getting Started
--------------------
### Prerequisites
In order to get started users will need to have:
* A Raspberry PI 2/3 B+
* Raspbian Installed 
* Script to interact with LEDs 

After installing the [ledkonfig.sh](https://bitbucket.org/stanleee/s3587412_a2/src/master/ledkonfig.sh), Run the following Command to launch the Led Konfigurator:


```./ledkongig.sh```


## Development
-------------------
In this project, it had been broke down into 6 task and each Task will work on a different functions as listed 

**Task 1** - Kernel Compilation

**Task 2** - Prints out the available LEDs in the PI 

**Task 3** - After redirect into the selected Leds, print and list all the options to manipulate the LEDs

**Task 4** - Enable to turn on/off for the selected LEDs

**Task 5** - Printed out list of system events to associate the selectedLEDs  

**Task 6** - Associate LED with the performance of a process.

**Task 7** - Unassociate an LED with performance monitoring.

**[All Commits Log](https://bitbucket.org/stanleee/s3587412_a2/commits/all)**


## Maintainers
--------------------
Current maintainers:

* Stanley Teh


## Useful Resources
----------------------
[Getting Started With Bash On The Raspberry Pi](https://learn.pimoroni.com/tutorial/bash/getting-started-with-bash)

[Week 2: Learning Materials/Activities](https://docs.google.com/document/d/1tJ7hACZT8yQPlEvsBm0o_xoOV0W_K-jiOwSK9A9AWzo/edit)

[README Template](https://www.drupal.org/docs/develop/documenting-your-project/readme-template)

 
